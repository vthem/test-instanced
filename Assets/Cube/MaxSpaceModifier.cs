﻿using UnityEngine;
using DG.Tweening;
using System;
using UnityEngine.Events;

public class MaxSpaceModifier : MonoBehaviour
{
    public DrawInstanced _drawInstanced;
    public float _duration = 10f;
    public float _startValue = 1000f;

    public UnityEvent _onMoveComplete;

    // Update is called once per frame
    void Start()
    {
        _drawInstanced._maxSpaceRandom = _startValue;
        DOTween.To(
            () => { return _drawInstanced._maxSpaceRandom; },
            ( v ) => { _drawInstanced._maxSpaceRandom = v; },
            1f,
            _duration )
            .SetEase( Ease.OutCirc )
            .OnComplete( () =>
            {
                gameObject.SetActive( false );
                if( _onMoveComplete != null )
                    _onMoveComplete.Invoke();
            } );
    }
}
