﻿using UnityEngine;

[ExecuteInEditMode]
public class DrawInstanced : MonoBehaviour
{

    public int _size = 1;
    public Mesh _mesh;
    public Material _material;
    public int _seed = 1;
    public bool _forceUpdate = false;
    public float _maxSpaceRandom = 1f;

    Matrix4x4[] _matrices;

    // Update is called once per frame
    void Update()
    {
        CheckMatrices();
        Graphics.DrawMeshInstanced( _mesh, 0, _material, _matrices );
    }

    private void OnValidate()
    {
        _size = Mathf.Max( 1, _size );
        _size = Mathf.Min( 10, _size );
    }

    void CheckMatrices()
    {
        if( _forceUpdate || _matrices == null || _matrices.Length != _size * _size * _size )
        {
            Random.InitState( _seed );
            _matrices = new Matrix4x4[ _size * _size * _size ];
            var mat = new Matrix4x4();
            var pos = Vector3.zero;
            for( int x = 0; x < _size; ++x )
            {
                for( int y = 0; y < _size; ++y )
                {
                    for( int z = 0; z < _size; ++z )
                    {
                        pos.x = Random.Range( 1f, _maxSpaceRandom ) * x;
                        pos.y = Random.Range( 1f, _maxSpaceRandom ) * y;
                        pos.z = Random.Range( 1f, _maxSpaceRandom ) * z;
                        mat.SetTRS( pos, Quaternion.identity, Vector3.one );
                        _matrices[ x + y * _size + z * _size * _size ] = mat;
                    }
                }
            }
        }
    }
}
