﻿using DG.Tweening;
using UnityEngine;

public class ShakeCube : MonoBehaviour
{
    public float _strength = 1f;
    public float _duration = 1f;

    public void ShakeIt()
    {
        if( !gameObject.activeInHierarchy )
            gameObject.SetActive( true );
        transform.DOShakePosition( _duration, _strength );
    }
}
